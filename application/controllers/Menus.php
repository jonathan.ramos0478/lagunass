<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {

	public function galerias()
	{
		$this->load->view('header');
		$this->load->view('menus/galerias');
		$this->load->view('footer');
	}
  public function eventos()
  {
    $this->load->view('header');
    $this->load->view('menus/eventos');
    $this->load->view('footer');
  }
  public function acercas()
  {
    $this->load->view('header');
    $this->load->view('menus/acercas');
    $this->load->view('footer');
  }
}
