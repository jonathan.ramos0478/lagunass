
<footer style="background-color:#C7C5C5;">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="about-veno">
                        <div class="logo">
                            <img src="<?php echo base_url(); ?>plantilla/img/footer_logo1.png" alt="Venue Logo">
                        </div>
                        <p>Latacunga, también conocida como  San Vicente Mártir de Latacunga, es una ciudad de Ecuador ; es la cabecera cantonal del Cantón Latacunga y capital de la Provincia de Cotopaxi, es la urbe más grande y poblada de la provincia. Se localiza al centro-norte de la Región interandina del Ecuador, en la hoya del río Patate, atravesada por los ríos Cutuchi y Pumacunchi, a una altitud de 2750 msnm y con un clima frío andino de 12 °C en promedio.</p>
                        <ul class="social-icons">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="useful-links">
                        <div class="footer-heading">
                            <h4>Nuestros Links</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li><a href="#"><i class="fa fa-stop"></i>Ayuda</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Registrate</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Login</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Mi Perfil</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Como Funciona?</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Más Acerca De Nosotros</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li><a href="#"><i class="fa fa-stop"></i>Nuestros Clientes</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Asociaciones</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Blog Entradas</a></li>
                                    <li><a href="#"><i class="fa fa-stop"></i>Contactos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="contact-info">
                        <div class="footer-heading">
                            <h4>Contactos Informacion</h4>
                        </div>
                        <p>Un contacto es la conexión o vínculo que se establece entre dos objetos o sujetos, real o virtual.

Dos objetos o sujetos están en contacto físico cuando son cercanos y en su totalidad o en parte se tocan.</p>
                        <ul>
                            <li><span>Telefono:</span><a href="#">370-0440 ext. 1303</a></li>
                            <li><span>Email:</span><a href="#">lalaguna@gmail.com</a></li>
                            <li><span>Direccion:</span><a href="#">Ecuador - Latacunga</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="sub-footer">
        <p>Copyright &copy; 2023 Company Name

    	- Designado: <a rel="nofollow" href="http://www.templatemo.com">Jhomara Parraga</a> Distribution: <a rel="nofollow" href="https://themewagon.com">Universidad Tecnica De Cotopaxi</a></p>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>plantilla/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="<?php echo base_url(); ?>plantilla/js/vendor/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>plantilla/js/datepicker.js"></script>
    <script src="<?php echo base_url(); ?>plantilla/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>plantilla/js/main.js"></script>
</body>
</html>
