<!DOCTYPE html>
<html>
<body style="background-color:#E3E3E3;">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Venue - Responsive Web Template</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/fontAwesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/hero-slider.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/owl-carousel.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/datepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <script src="<?php echo base_url(); ?>/plantilla/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!--
	Venue Template
	http://www.templatemo.com/tm-522-venue
-->
    </head>

<body>

    <div class="wrap">
        <header id="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <button id="primary-nav-button" type="button">Menu</button>
                        <a href="index.html"><div class="logo">
                            <img src="<?php echo base_url(); ?>/plantilla/img/logo1.png" alt="Venue Logo">
                        </div></a>
                        <nav id="primary-nav" class="dropdown cf">
                            <ul class="dropdown menu">
                                <li class='active'><a href="<?php echo base_url(); ?>">Inicio</a></li>
                                <li><a href="<?php echo site_url(); ?>/menus/acercas">Acerca De</a>
                                </li>
                                <li><a href="<?php echo site_url(); ?>/menus/galerias">Galeria</a></li>
                                <li><a href="<?php echo site_url(); ?>/menus/eventos">Eventos</a></li>
                                <li><a class="scrollTo" data-scrollTo="services" href="#">Servicios</a></li>
                            </ul>
                        </nav><!-- / #primary-nav -->
                    </div>
                </div>
            </div>
        </header>
    </div>
