<section class="banner" id="top">
		<div class="container">
				<div class="row">
						<div class="col-md-10 col-md-offset-1">
								<div class="banner-caption">
										<div class="line-center"></div>
										<h2 class="text-dec<?php echo base_url(); ?>plantilla/">Parque Náutico La Laguna</h2>
										<span>Descubre mas sobre el atractivo Parque Náutico La Laguna </span>
										<div class="blue-button">
												<a class="scrollTo" data-scrollTo="popular" href="#">Ver Mas</a>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>

<section class="popular-places" id="popular">
		<div class="container-fluid">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Lugares Populares</span>
										<h2>Atractivos De La Ciudad</h2>
								</div>
						</div>
				</div>
				<div class="owl-carousel owl-theme">
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_1.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Vicente Leon</h4>
												<span>76 Photos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_2.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>La Laguna</h4>
												<span>18 Photos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_3.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Santo Domingo</h4>
												<span>55 Photos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_4.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Muelle Laguna</h4>
												<span>66 Photos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
								<div class="thumb">
										<img src="<?php echo base_url(); ?>plantilla/img/popular_item_5.jpeg" width="244px" height="249px" alt="">
										<div class="text-content">
												<h4>Municipio</h4>
												<span>82 Photos</span>
										</div>
										<div class="plus-button">
												<a href="#"><i class="fa fa-plus"></i></a>
										</div>
								</div>
						</div>
						<div class="item popular-item">
                    <div class="thumb">
                        <img src="<?php echo base_url(); ?>plantilla/img/popular_item_6.jpeg" width="244px" height="249px" alt="">
                        <div class="text-content">
                            <h4>El Salto</h4>
                            <span>76 Photos</span>
                        </div>
                        <div class="plus-button">
                            <a href="#"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="item popular-item">
                    <div class="thumb">
                        <img src="<?php echo base_url(); ?>plantilla/img/popular_item_7.jpeg" width="244px" height="249px" alt="">
                        <div class="text-content">
                            <h4>La Estación</h4>
                            <span>36 Photos</span>
                        </div>
                        <div class="plus-button">
                            <a href="#"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="item popular-item">
                    <div class="thumb">
                        <img src="<?php echo base_url(); ?>plantilla/img/popular_item_8.jpeg" width="244px" height="249px" alt="">
                        <div class="text-content">
                            <h4>Cotopaxi</h4>
                            <span>48 Photos</span>
                        </div>
                        <div class="plus-button">
                            <a href="#"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="item popular-item">
                    <div class="thumb">
                        <img src="<?php echo base_url(); ?>plantilla/img/popular_item_9.jpeg" width="244px" height="249px" alt="">
                        <div class="text-content">
                            <h4>San Agustin</h4>
                            <span>66 Photos</span>
                        </div>
                        <div class="plus-button">
                            <a href="#"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="item popular-item">
                    <div class="thumb">
                        <img src="<?php echo base_url(); ?>plantilla/img/popular_item_10.jpeg" width="244px" height="249px" alt="">
                        <div class="text-content">
                            <h4>Casa Marqueses</h4>
                            <span>85 Photos</span>
                        </div>
                        <div class="plus-button">
                            <a href="#"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>

			</div>
		</div>
</section>



<section class="featured-places" id="blog">
		<div class="container">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Fechas Festivas</span>
										<h2>Festividades De La Ciudad</h2>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="featured-item">
										<div class="thumb">
												<img src="img/featured_item_1.jpg" alt="">
												<div class="overlay-content">
														<ul>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
														</ul>
												</div>
												<div class="date-content">
														<h6>05</h6>
														<span>Noviembre</span>
												</div>
										</div>
										<div class="down-content">
												<h4>La Mama Negra</h4>
												<span>Categoria Uno</span>
												<p>La Santísima Tragedia, popularmente conocida Mama Negra, es una fiesta que se realiza, dos veces al año, en la ciudad ecuatoriana de Latacunga, capital de la Provincia de Cotopaxi.</p>
												<div class="row">
														<div class="col-md-6 first-button">
																<div class="text-button">
																		<a href="#">Añadir Favoritos</a>
																</div>
														</div>
														<div class="col-md-6">
																<div class="text-button">
																		<a href="#">Continuar Leyendo</a>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="featured-item">
										<div class="thumb">
												<img src="img/featured_item_2.jpg" alt="">
												<div class="overlay-content">
														<ul>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
														</ul>
												</div>
												<div class="date-content">
														<h6>28</h6>
														<span>Enero</span>
												</div>
										</div>
										<div class="down-content">
												<h4>Bailes de Inocentes</h4>
												<span>Categoria dos</span>
												<p>Los obreros se disfrazaban y ridiculizaban al patrón “esa fiesta salió del entorno de la hacienda, se socializó en las familias sangabrieleñas, por sectores, en 1940, se generalizó la fiesta y se extendía, hasta el mes de marzo.</p>
												<div class="row">
														<div class="col-md-6 first-button">
																<div class="text-button">
																		<a href="#">Añadir Favoritos</a>
																</div>
														</div>
														<div class="col-md-6">
																<div class="text-button">
																		<a href="#">Continuar Leyendo</a>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="featured-item">
										<div class="thumb">
												<img src="img/featured_item_3.jpg" alt="">
												<div class="overlay-content">
														<ul>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
														</ul>
												</div>
												<div class="date-content">
														<h6>11</h6>
														<span>Noviembre</span>
												</div>
										</div>
										<div class="down-content">
												<h4>Virgen de Las Mercedes</h4>
												<span>Categoria Tres</span>
												<p>El religioso indicó que por segunda ocasión la imagen sobrevolará en un helicóptero desde donde se impartirá la bendición al cantón Latacunga..</p>
												<div class="row">
														<div class="col-md-6 first-button">
																<div class="text-button">
																		<a href="#">Añadir Favoritos</a>
																</div>
														</div>
														<div class="col-md-6">
																<div class="text-button">
																		<a href="#">Continuar Leyendo</a>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>



<section class="our-services" id="services">
		<div class="container">
				<div class="row">
						<div class="col-md-12">
								<div class="section-heading">
										<span>Preguntas</span>
										<h2>Preguntas Frecuentes</h2>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-12">
								<div class="down-services">
										<div class="row">
												<div class="col-md-5 col-md-offset-1">
														<div class="left-content">
																<h4>¿Qué es la Mama Negra?</h4>
																<p>La Mama Negra, conocida como Santísima Tragedia es una fiesta tradicional propia de la ciudad de Latacunga, simbiosis de las culturas indígena, española y africana en la cual sus habitantes rinden homenaje a la Virgen de Las Mercedes como demostración de agradecimiento por los favores concedidos.</p>
																<div class="blue-button">
																		<a href="#">Descubre Más</a>
																</div>
														</div>
												</div>
												<div class="col-md-5">
														<div class="accordions">
																<ul class="accordion">
																		<li>
																				<a>¿Qué se hace en la Mama Negra?</a>
																				<p>La Mama Negra es un desfile de varios personajes que se celebra dos veces al año. En septiembre en honor a la Virgen de La Merced y en noviembre, como un desfile de personalidades destacadas en celebración del aniversario de Independencia de la ciudad.</p>
																		</li>
																		<li>
																				<a>¿Cuándo es la Mama Negra en Ecuador?</a>
																				<p>Latacunga celebra el próximo 11 de noviembre sus 202 años de independencia, por lo que en el marco de esta conmemoración se realizará la Fiesta de la Santísima Tragedia, Capitanía o Mama Negra, programada para el 5 de noviembre.</p>
																		</li>
																		<li>
																				<a>¿Cuáles son los personajes principales de la Mama Negra?</a>
																				<p>Los personajes principales de las dos fiestas son: la Mama Negra, el Capitán, el Ángel de la Estrella, el Abanderado y el Rey Moro, En la fiesta de septiembre aparece el Embajador, los payasos y los urcuyayas.</p>
																		</li>
																</ul> <!-- / accordion -->
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>



<section id="video-container">
		<div class="video-overlay"></div>
		<div class="video-content">
				<div class="inner">
						<span>Video Presentación</span>
						<h2>La Mama Negra Latacunga - Ecuador</h2>
						<a href="https://www.youtube.com/watch?v=GKz0kSjAi0w" target="_blank"><i class="fa fa-play"></i></a>
				</div>
		</div>
		<video autoplay="" loop="" muted>
			<source src="<?php echo base_url(); ?>plantilla/highway-loop.mp4" type="video/mp4" />
		</video>
</section>


<section class="contact" id="contact">
		<div id="map">
					<!-- How to change your own map point
											 1. Go to Google Maps
											 2. Click on your location point
											 3. Click "Share" and choose "Embed map" tab
											 4. Copy only URL and paste it within the src="" field below
								-->
								<iframe
								       src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=6406 Parte interior del parque, Calle Caraihuayrazo, Latacunga"
								       width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
</section>
